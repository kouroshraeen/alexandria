# Alexandria

Alexandria is part of the second student project for the Udacity Android Developer Nanodegree program.

This application gets book information from a Google API. All titles, cover images, and author information come from there.
The original version of this app was built by Sascha Jaschke and was modified by Kourosh Raeen.

## Screenshots
![screen](./art/navigation.png)
![screen](./art/add-book.png)
![screen](./art/scan-result.png)
![screen](./art/book-list.png)
![screen](./art/book-detail-1.png)
![screen](./art/book-detail-2.png)
![screen](./art/book-detail-3.png)
![screen](./art/book-detail-4.png)

## Additions

* Barcode scanning functionality using Google Mobile Vision API
* Added a check for internet connectivity before searching for a book
* String resources are extracted into the strings.xml file
* Untranslatable strings have a translatable tag marked to false
* Refactored to use the Picasso library for image downloading and caching

## Bug fixes

* List of books is refreshed after deleting a book
* Fixed the delete action in AddBook.java when ISBN is entered using the keyboard and "978" is not typed in

## Libraries

* [Picasso](https://github.com/square/picasso)

